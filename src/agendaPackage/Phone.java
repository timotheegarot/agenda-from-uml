
import java.util.regex.*;

public class Phone extends ContactDetail {

  public String phoneNumber;

  public Phone(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @Override
  public void validate() {
      // ^
      //   (?:(?:\+|00)33|0)     # Dialing code
      //   \s*[1-9]              # First number (from 1 to 9)
      //   (?:[\s.-]*\d{2}){4}   # End of the phone number
      // $

    // String validationRegex "^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.]*\d{2}){4}$";
    // Pattern p = Pattern.compile(validationRegex);
    throw new UnsupportedOperationException("Unimplemented method 'validate'");
  }
}
