
public class Address extends ContactDetail {

  public String streetName;
  public String streetNumber;
  public String city;

  public Address(String streetName, String streetNumber, String city) {
    this.streetName = streetName;
    this.streetNumber = streetNumber;
    this.city = city;
  }

  @Override
  public void validate() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'validate'");
  }
}
