
public class Main {
  public static void main(String[] args) {
    User ziad = new User();
    Agenda agenda = new Agenda("ZiadAgenda");
    ziad.agendas.add(agenda);
    
    Contact louisa = new Contact("Louisa", "Ziad");
    louisa.addEmail("louisa@email.com");
    louisa.addAddress("Rue de la Paix", "13", "Rennes");
    louisa.addPhone("0606060606");
    louisa.addWebsite("https://louisa.com");
    agenda.addContact(louisa);
  }
}
