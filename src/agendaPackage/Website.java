
import java.util.regex.*;

public class Website extends ContactDetail {

  public String websiteURL;

  public Website(String websiteURL) {
    this.websiteURL = websiteURL;
  }

  @Override
  public void validate() {
    String validationRegex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    Pattern p = Pattern.compile(validationRegex);
    throw new UnsupportedOperationException("Unimplemented method 'validate'");
  }
}
