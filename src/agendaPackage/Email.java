
import java.util.regex.*;

public class Email extends ContactDetail {

  public String emailAddress;

  public Email(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  @Override
  public void validate() {
    String validationRegex = "^(.+)@(.+)$";
    Pattern p = Pattern.compile(validationRegex);
    throw new UnsupportedOperationException("Unimplemented method 'validate'");
  }
}
