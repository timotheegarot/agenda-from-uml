
import java.util.ArrayList;

public class User {

  public String name;
  public String login;
  public ArrayList<Agenda> agendas;
  private String password;
  private String jwt_token;
  User user = new User();

  public User() {
    this.agendas = new ArrayList<Agenda>();
  }

  public void addUser(String name, String login, String password, String jwt_token) {
    this.name = name;
    this.login = login;
    this.password = password;
    this.jwt_token = jwt_token;
  }

  // public void deleteUser() {
    
  // }
}
