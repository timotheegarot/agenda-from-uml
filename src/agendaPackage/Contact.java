import java.util.ArrayList;

public class Contact {

  public String firstname;
  public String lastname;
  public String emailAddress;
  public String streetName;
  public String streetNumber;
  public String city;  
  public String phone;
  public String websiteURL;
  private ArrayList<ContactDetail> contacts;

  public Contact(String firstname, String lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.contacts = new ArrayList<ContactDetail>();
  } 

  public void addContact(ContactDetail c) {
    this.contacts.add(c);
  }

  public void addEmail(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public void addAddress(String streetName, String streetNumber, String city) {
    this.streetName = streetName;
    this.streetNumber = streetNumber;
    this.city = city;
  }

  public void addPhone(String phone) {
    this.phone = phone;
  }

  public void addWebsite(String websiteURL) {
    this.websiteURL = websiteURL;
  }

  public void deleteContact() {
    contacts.clear();
  }
}
