
import java.util.ArrayList;

public class Agenda {

  public String label;
  public ArrayList<Contact> contacts;

  public Agenda(String label) {
    this.label = label;
    this.contacts = new ArrayList<Contact>();
  }

  public void addContact(Contact contact) {
    this.contacts.add(contact);
  }

  public void deleteAgenda() {
    contacts.clear();
  }
}
