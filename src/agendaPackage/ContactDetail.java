
public abstract class ContactDetail {

  protected String value;

  public abstract void validate();
}
