# Agenda-from-UML

## Description du projet

A partir d'un diagramme de classes, implémenter les les composants de la couche métier de l'application, dans le langage de notre choix (ici Java).

## Installer le projet

Version du JDK : 17.0.5  
Vérifier la version du jdk et de Java :  
`javac -version` et `java -version`

## Lancer un environnement de développement

`apt-get install openjdk-17-jdk openjdk-17-jre`

Ajouter `C:\Program Files (x86)\Java\jdk\bin` aux variables d'environnement (Windows).

## Lancer le projet

Exécuter le fichier agenda-from-uml.jar (double-clic ou ligne de commandes).  
En ligne de commandes :  
`java -jar agenda-from-uml.jar`

## Diagramme de classes

![Diagramme de classes](./Capture.PNG)
